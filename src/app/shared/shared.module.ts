import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavService } from './services/nav.service';
import { UtilsService } from './services/utils.service';
import { RouterModule } from '@angular/router';
import { NotifierModule } from 'angular-notifier';
import { NgDatepickerModule } from 'ng2-datepicker';
import { UserService } from './services/user.service';
import { AuthInterceptor } from '../auth/interceptors/auth.interceprot';
import { FlightService } from './services/flight.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    RouterModule,
    NgDatepickerModule
  ],
  declarations: [
    NavbarComponent,
    FooterComponent
  ],
  providers: [
    NavService,
    UtilsService,
    UserService,
    FlightService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    RouterModule,
    NgDatepickerModule
  ]
})
export class SharedModule { }
