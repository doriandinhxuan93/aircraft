export interface Serialize<T> {
    parse(data: any): T;
    serialize(): any;
}