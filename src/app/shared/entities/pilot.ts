import { Aircraft } from "./aircraft";
import { License } from "./license";
import { Serialize } from "../interfaces/serialize";
import { Entity } from "../classes/entity";
import * as _ from "lodash";

export class Pilot extends Entity implements Serialize<Pilot>{
    private TAG: string = '[Pilot]';
    public id: string;
    public flightHours: number;
    public licenses: Array<License> = [];
    public aircraft: Array<Aircraft> = [];
    public dateOfBirth: string;

    parse(data: any): Pilot {
        if (_.isNull(data)) throw "Invalid license data";

        this.id = _.get(data, "_id");
        this.flightHours = _.get(data, "flightHours");
        this.dateOfBirth = _.get(data, "dateOfBirth");
        _.get(data, "licenses", []).map(l => this.licenses.push(new License().parse(l)));
        _.get(data, "aircraft", []).map(a => this.aircraft.push(new Aircraft().parse(a)));

        console.log(`${this.TAG} Parsed successfully`, this);

        return this;
    }

    serialize(): any {
        let serialized = {
            id: this.id,
            flightHours: this.flightHours,
            licenses: this.licenses,
            aircraft: this.aircraft,
            dateOfBirth: this.dateOfBirth,
        };

        return serialized;
    }
}