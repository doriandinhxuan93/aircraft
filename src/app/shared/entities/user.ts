import { Roles } from '../enums/roles';
import { Serialize } from '../interfaces/serialize';
import { Entity } from '../classes/entity';
import * as _ from 'lodash';
import { Pilot } from './pilot';
import { Aircraft } from './aircraft';
import { License } from './license';

export class User extends Entity implements Serialize<User>{
    public id: string;
    public name: string;
    public email: string;
    public surname: string;
    public role: Roles;
    public birthday: Date;
    public flightHours: Number;
    public aircrafts: Array<Aircraft> = [];
    public licenses: Array<License> = [];

    parse(data: any): User {
        if (_.isNull(data)) throw new Error('Invalid license data');

        this.id = _.get(data, 'id', '');
        this.name = _.get(data, 'name', '');
        this.email = _.get(data, 'email', '');
        this.surname = _.get(data, 'surname', '');
        this.role = _.get(data, 'role', '');
        this.birthday = _.get(data, 'birthday', new Date());
        this.flightHours = _.get(data, 'flightHours', 0);
        _.get(data, 'aircrafts', '').map((a: Aircraft) => this.aircrafts.push(new Aircraft().parse(a)));
        _.get(data, 'licenses', '').map((l: License) => this.licenses.push(new License().parse(l)));

        return this;
    }

    serialize(): any {
        const serialized = {
            id: this.id,
            name: this.name,
            email: this.email,
            surname: this.surname,
            role: this.role,
            birthday: this.birthday,
            flightHours: this.flightHours,
            aircrafts: this.aircrafts,
            licenses: this.licenses
        };

        return serialized;
    }

    get getUserName(): string {
        return `${this.name} ${this.surname}`;
    }

}
