import { Serialize } from '../interfaces/serialize';
import { Entity } from '../classes/entity';
import * as _ from 'lodash';

export class Aircraft extends Entity implements Serialize<Aircraft> {

    public id: string;
    public name: string;
    public type: string;
    public totalHours: number;

    parse(data: any): Aircraft {
        if (_.isNull(data)) throw new Error('Invalid jwt token');

        this.id = _.get(data, '_id');
        this.name = _.get(data, 'name');
        this.type = _.get(data, 'type');
        this.totalHours = _.get(data, 'totalHours');

        return this;
    }

    serialize(): any {
        const serialized = {
            id: this.id,
            name: this.name,
            type: this.type,
            totalHours: this.totalHours
        };

        return serialized;
    }
}