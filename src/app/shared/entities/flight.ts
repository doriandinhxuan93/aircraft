import { Serialize } from 'src/app/shared/interfaces/serialize';
import { Entity } from 'src/app/shared/classes/entity';
import { Pilot } from './pilot';
import { Aircraft } from './aircraft';
import { Difficulty } from '../enums/difficulty';
import * as _ from 'lodash';
import { User } from './user';

export class Flight extends Entity implements Serialize<Flight> {

    public id: string;
    public user?: User;
    public takeoff: string;
    public touchdown: string;
    public date: string;
    public description: string;
    public time: number;
    public price: number;
    public aircraft: Aircraft;
    public difficulty: Difficulty;

    parse(data: any): Flight {
        if (_.isNull(data)) throw new Error('Invalid flight data');

        this.id = _.get(data, '_id');
        if (!_.isUndefined(_.get(data, '_creator'))) {
            this.user = new User().parse(_.get(data, '_creator', {}));
        }
        this.takeoff = _.get(data, 'takeoff');
        this.touchdown = _.get(data, 'touchdown');
        this.date = _.get(data, 'date');
        this.description = _.get(data, 'description');
        this.time = _.get(data, 'time');
        this.price = _.get(data, 'price');
        this.aircraft = new Aircraft().parse(_.get(data, 'aircraft'));
        this.difficulty = _.get(data, 'difficulty');

        return this;
    }

    serialize(): any {
        const serialized = {
            id: this.id,
            user: this.user,
            takeoff: this.takeoff,
            touchdown: this.touchdown,
            date: this.date,
            description: this.description,
            time: this.time,
            price: this.price,
            aircraft: this.aircraft,
            difficulty: this.difficulty
        };

        return serialized;
    }
}
