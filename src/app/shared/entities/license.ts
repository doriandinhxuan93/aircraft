import { Serialize } from '../interfaces/serialize';
import { Entity } from '../classes/entity';
import * as _ from 'lodash';

export class License extends Entity implements Serialize<License> {
    public id: string;
    public name: string;
    public date: string;

    parse(data: any): License {
        if (_.isNull(data)) throw new Error('Invalid license data');

        this.id = _.get(data, '_id');
        this.name = _.get(data, 'name');
        this.date = _.get(data, 'date');

        return this;
    }

    serialize(): any {
        let serialized = {
            id: this.id,
            name: this.name,
            date: this.date
        };

        return serialized;
    }
}
