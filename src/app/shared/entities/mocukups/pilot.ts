import * as L  from "./license";
import { Pilot } from "../pilot";
import * as A  from "./aircraft";

export var PilotMOCK: Array<any> = [
    {
        id: '1',
        flightHours: 100,
        licenses: [L.LicenseMOCK[0], L.LicenseMOCK[4]],
        aircraft: [A.AircraftMOCK[1], A.AircraftMOCK[2]],
        dateOfBirth: '10/10/1992'
    },
    {
        id: '2',
        flightHours: 12,
        licenses: [L.LicenseMOCK[0], L.LicenseMOCK[4]],
        aircraft: [A.AircraftMOCK[1], A.AircraftMOCK[2]],
        dateOfBirth: '10/10/1982'
    },
    {
        id: '3',
        flightHours: 15,
        licenses: [L.LicenseMOCK[0], L.LicenseMOCK[4]],
        aircraft: [A.AircraftMOCK[1], A.AircraftMOCK[2]],
        dateOfBirth: '10/10/1988'
    },
    {
        id: '4',
        flightHours: 140,
        licenses: [L.LicenseMOCK[0], L.LicenseMOCK[4]],
        aircraft: [A.AircraftMOCK[1], A.AircraftMOCK[2]],
        dateOfBirth: '10/10/1991'
    }
]


