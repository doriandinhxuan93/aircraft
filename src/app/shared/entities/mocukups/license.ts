import { Flight } from "../flight";
import { Difficulty } from "../../enums/difficulty";
import { License } from "../license";


export var LicenseMOCK: Array<any> = [
    {
        id: '1',
        name: 'nazwa licencji 1',
        date: '10/10/1992',
        institution: 'aaaaaa'
    },
    {
        id: '2',
        name: 'nazwa licencji 2',
        date: '10/10/1992',
        institution: 'aaaaaa'
    },
    {
        id: '3',
        name: 'nazwa licencji 3',
        date: '10/10/1992',
        institution: 'aaaaaa'
    },
    {
        id: '4',
        name: 'nazwa licencji 4',
        date: '10/10/1992',
        institution: 'aaaaaa'
    },
    {
        id: '5',
        name: 'nazwa licencji 5',
        date: '10/10/1992',
        institution: 'aaaaaa'
    }
]

