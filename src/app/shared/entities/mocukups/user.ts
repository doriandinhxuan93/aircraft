import * as L from "./license";
import { Pilot } from "../pilot";
import * as A from "./aircraft";
import * as P from "./pilot";
import { Roles } from "../../enums/roles";

export var UserMOCK: Array<any> = [
    {
        id: '1',
        name: 'Jan',
        surname: 'Kowalski',
        role: Roles.PILOT,
        pilot: P.PilotMOCK[0]
    },
    {
        id: '2',
        name: 'Piotr',
        surname: 'Rogucki',
        role: Roles.PILOT,
        pilot: P.PilotMOCK[1]
    },
    {
        id: '3',
        name: 'Jan',
        surname: 'Kowalski',
        role: Roles.PILOT,
        pilot: P.PilotMOCK[2]
    },
    {
        id: '4',
        name: 'Jan',
        surname: 'Kowalski',
        role: Roles.PILOT,
        pilot: P.PilotMOCK[3]
    },
]