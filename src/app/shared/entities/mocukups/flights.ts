import { Flight } from "../flight";
import { Difficulty } from "../../enums/difficulty";
import * as U  from "./user";
import * as A  from "./aircraft";

export var FlightsMOCK: Array<any> = [
    {
        id: '1',
        user: U.UserMOCK[0],
        place: 'Warszawa',
        date: '11/11/2018',
        description: 'In labore sit quis quis consectetur et sint ad elit duis. Anim ipsum pariatur qui dolore officia culpa in deserunt exercitation cillum labore dolor. Adipisicing ad adipisicing ipsum veniam cupidatat anim exercitation voluptate dolore reprehenderit tempor. Mollit enim exercitation reprehenderit aliquip tempor enim laboris elit esse duis fugiat eu veniam. In esse et cupidatat ut enim in esse cillum ex eu duis anim.',
        time: 2,
        price: 200,
        aircraft: A.AircraftMOCK[0],
        difficulty: Difficulty.EASY
    },
    {
        id: '2',
        user: U.UserMOCK[1],
        place: 'Warszawa',
        date: '11/11/2018',
        description: 'Fugiat culpa est et magna cillum commodo commodo nulla esse veniam deserunt adipisicing. In cupidatat et elit exercitation ullamco laboris ad ullamco esse. In irure nulla proident mollit magna magna excepteur et pariatur dolor Lorem Lorem veniam cillum. Aute ullamco eiusmod do deserunt. Duis magna et proident consequat eu. Magna sit mollit ipsum non pariatur est velit duis exercitation.',
        time: 1,
        price:600,
        aircraft: A.AircraftMOCK[1],
        difficulty: Difficulty.EASY
    },
    {
        id: '3',
        user: U.UserMOCK[2],
        place: 'Warszawa',
        date: '11/11/2018',
        description: 'Amet Lorem aute anim et. Ullamco incididunt sit excepteur excepteur id Lorem qui culpa sint occaecat dolore adipisicing anim. Anim aliquip voluptate cillum fugiat ex duis. Pariatur excepteur culpa ad nisi ea sit laborum labore Lorem excepteur ad. Aliquip sint ipsum laboris officia. Irure sit sunt aute qui occaecat. Officia Lorem anim laborum non sint.',
        time: 4,
        price: 2100,
        aircraft: A.AircraftMOCK[2],
        difficulty: Difficulty.EASY
    },
    {
        id: '4',
        user: U.UserMOCK[3],
        place: 'Warszawa',
        date: '11/11/2018',
        description: 'Aliquip proident cupidatat sit pariatur ipsum ea voluptate.',
        time: 2,
        price: 200,
        aircraft: A.AircraftMOCK[3],
        difficulty: Difficulty.EASY
    }
]