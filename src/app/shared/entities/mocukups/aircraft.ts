import { Flight } from "../flight";
import { Difficulty } from "../../enums/difficulty";
import { Aircraft } from "../aircraft";

export var AircraftMOCK: Array<any> = [
    {
        id: '1',
        name: 'name 1',
        year: 2019,
    },
    {
        id: '2',
        name: 'name 2',
        year: 2019,
    },
    {
        id: '3',
        name: 'name 3',
        year: 2019,
    },
    {
        id: '4',
        name: 'name 4',
        year: 2019,
    },
    {
        id: '5',
        name: 'name 5',
        year: 2019,
    },

]