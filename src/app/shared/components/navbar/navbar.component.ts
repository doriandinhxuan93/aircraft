import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { RoutingValues, Routing } from '../../enums/routing';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {}

  public isAuthenticated = () => this.authService.isAuthenticated();

  get routingValues() {
    return Routing;
  }

  test() {
    return '/login';
  }
}
