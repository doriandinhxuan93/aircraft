export class Entity {
    public serialize() {
        return {};
    }

    public toJson(spaces = 2) {
        const data = this.serialize();
        return JSON.stringify(data, null, spaces);
    }
}