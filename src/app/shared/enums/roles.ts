export enum Roles {
    SUPER_ADMIN = 'SUPER_ADMIN',
    ADMIN = 'ADMIN',
    USER = 'USER',
    PILOT = 'PILOT',
    VIEWER = 'VIEWER'
}

export const RolesOptions = [ Roles.SUPER_ADMIN, Roles.ADMIN, Roles.USER, Roles.PILOT, Roles.VIEWER ];
