export enum Routing {
    APP = 'app',
    LOGIN = 'login',
    REGISTER = 'register',
    DASHBOARD = 'dashboard',
    ADD_FLIGHT = 'add',
    SETTINGS = 'settings',
    FLIGHTS = 'flights'
}

export const RoutingValues = [ Routing.LOGIN, Routing.REGISTER, Routing.DASHBOARD, Routing.ADD_FLIGHT, Routing.SETTINGS, Routing.FLIGHTS ];
