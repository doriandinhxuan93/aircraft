export enum Difficulty {
    HARD = 'HARD',
    MEDIUM = 'MEDIUM',
    EASY = 'EASY'
}

export const DifficultyOptions = [ Difficulty.HARD, Difficulty.MEDIUM, Difficulty.EASY ];
