import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class UtilsService {

    constructor() { }

    public httpHeaders(): Object {
        const token: string = localStorage.getItem('authorizationToken') as string;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                // 'Authorization': (token) ? token : ''
            })
        };
        return httpOptions;
    }

    public interpolate(template: string, params: {}) {
        const names = Object.keys(params);
        const vals = Object.values(params);
        return new Function(...names, `return \`${template}\`;`)(...vals);
    }
}
