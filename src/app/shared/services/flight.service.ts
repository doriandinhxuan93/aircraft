import { Injectable } from '@angular/core';
import { User } from '../entities/user';
import { Observable, of } from 'rxjs';
import { UtilsService } from './utils.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators/catchError';
import * as Rx from 'rxjs';
import { Flight } from '../entities/flight';

@Injectable()
export class FlightService {

  static FLIGHT = 'flights/my';

  private TAG = '[FlightService]';

  private flightsSource: Rx.Subject<Array<Flight>> = new Rx.Subject<Array<Flight>>();
  public flights: Observable<Array<Flight>> = this.flightsSource.asObservable();

  constructor(private http: HttpClient, private utilsService: UtilsService) { }

  my(): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${FlightService.FLIGHT}`, {});
    return this.http.get(endpoint, this.utilsService.httpHeaders()).pipe(
      map((response) => {

        const flights = [];
        const rawFlights = response['_body'];

        rawFlights.map((flight: any) => flights.push(new Flight().parse(flight)));

        this.flightsSource.next(flights);
        console.log(this.TAG, '[Response] flights', flights);
        return flights;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
      })
    );
  }
}
