import { Injectable } from '@angular/core';
import { User } from '../entities/user';
import { Observable, of } from 'rxjs';
import { UtilsService } from './utils.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators/catchError';
import * as Rx from 'rxjs';

@Injectable()
export class UserService {

  static USER = 'user/my';

  private TAG = '[UserService]';

  private userSource: Rx.Subject<User> = new Rx.Subject<User>();
  public user: Observable<User> = this.userSource.asObservable();

  constructor(private http: HttpClient, private utilsService: UtilsService) { }

  my(): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${UserService.USER}`, {});

    return this.http.get(endpoint, this.utilsService.httpHeaders()).pipe(
      map((response) => {
        const user = new User().parse(response['_body']);
        this.userSource.next(user);
        console.log(this.TAG, '[Response] user', user);
        return response;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
      })
    );
  }
}
