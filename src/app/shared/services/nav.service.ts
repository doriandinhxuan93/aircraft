import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class NavService {

    constructor(private _router: Router, private location: Location) {
    }

    goToPage(pageId: any = '/', params?: Object, queryParams?: Object): Promise<boolean> {
        return this._router.navigate(params ? [pageId, params] : [pageId], queryParams ? { queryParams: queryParams } : {});
    }

    reload() {
        location.replace('/');
    }

    reloadToPage(pageId: any = '/', params?: Object, queryParams?: Object) {
        location.replace(pageId);
    }

    navigateBack() {
        this.location.back();
    }

    get router(): Router {
        return this._router;
    }
}
