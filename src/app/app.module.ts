import { NgModule, ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { HomeModule } from './home/home.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './home/home.component';
import { NotifierModule } from 'angular-notifier';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'app' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },

  { path: '**', pathMatch: 'full', redirectTo: 'app'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {enableTracing: false});

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    DashboardModule,
    AuthModule,
    SharedModule,
    HomeModule,
    routing,
    NotifierModule.withConfig({
    position: {
      horizontal: {
        position: 'middle',
        distance: 12
      },
      vertical: {
        position: 'bottom',
        distance: 0,
      }
    },
    behaviour: {
      autoHide: 4000,
      stacking: 1
    }
  }),
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [RouterModule, BrowserModule]
})

export class AppModule { }
