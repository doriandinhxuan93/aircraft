import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { NavService } from 'src/app/shared/services/nav.service';
import { NotifierService } from 'angular-notifier';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private navService: NavService, private notifierService: NotifierService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      console.log(state);
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.notifierService.notify('warning', 'Twoja sesja wygasła zaloguj się raz jeszcze');
      this.navService.goToPage('auth/login', {}, {url: state.url});
    }
  }
}
