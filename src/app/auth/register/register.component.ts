import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Roles, RolesOptions } from 'src/app/shared/enums/roles';
import { AuthService } from '../services/auth.service';
import { NavService } from 'src/app/shared/services/nav.service';
import { catchError } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private TAG = '[RegisterComponent]';

  public registerForm: FormGroup;
  public errorMessage: string;
  public submitted: boolean = false;

  get roles() {
    return Roles;
  }

  constructor(
    private authService: AuthService,
    private navService: NavService,
    private notifierService: NotifierService) { }


  ngOnInit() {
    this.initRegisterForm();
  }

  public initRegisterForm(): void {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, this.weakPassword]),
      confirmPassword: new FormControl(null, Validators.required),
      role: new FormControl(Roles.USER, Validators.required)
    }, {validators: this.checkPasswords });
  }

  public onSubmit(): void {
    if (!this.registerForm.valid) return;

    this.submitted = true;
    const body = this.registerForm.getRawValue();

    this.authService.register(body)
      .subscribe(
        (data) => {
          console.log(`${this.TAG} onSubmit - success`, data);
          this.navService.goToPage('/dashboard/settings');
        },
        (err) => {
          console.error(`${this.TAG} onSubmit - error`, err);
          this.errorMessage = err.error.message;
          this.notifierService.notify( 'error', err.error.message );
          this.submitted = false;
        },
        () => {
          console.log(`${this.TAG} onSubmit - completed`);
          this.registerForm.reset();
          this.submitted = false;
        });
  }

  public checkPasswords(group: FormGroup) {
    const password = group.controls.password.value;
    const confirmPassword = group.controls.confirmPassword.value;
    return password === confirmPassword ? null : { notSame: true };
  }

  public weakPassword(input: AbstractControl) {
    if (!input.value) return;

    const password = input.value;
    return password.length > 5 ? null : { weak: true };
  }
}
