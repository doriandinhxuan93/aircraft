import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { ILogin } from '../interfaces/login.interface';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { IToken } from '../interfaces/token.interface';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NavService } from 'src/app/shared/services/nav.service';
import { Routing } from 'src/app/shared/enums/routing';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static LOGIN = 'auth/signin';
  static REGISTER = 'auth/register';

  private TAG = '[PaymentsService]';

  private host = '';
  private prefix = '';

  constructor(
    private http: HttpClient,
    private utilsService: UtilsService,
    private navService: NavService) { }

  public login(body: ILogin): Observable<boolean | HttpErrorResponse> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${AuthService.LOGIN}`, {});
    return this.http.post(endpoint, body, this.utilsService.httpHeaders()).pipe(
      map((response) => {
        console.log(this.TAG, '[Response] login', response);
        const token: IToken = response['_body'];
        this.setAuthorizationToken(token.authorizationToken);
        return true;
      }),
      catchError((err: HttpErrorResponse) => {
        throw err;
      })
    );
  }

  public register(body: ILogin): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${AuthService.REGISTER}`, {});
    return this.http.post(endpoint, body, this.utilsService.httpHeaders()).pipe(
      map((response) => {
        console.log(this.TAG, '[Response] login', response);
        const token: IToken = response['_body'];

        if (token.authorizationToken) {
          this.setAuthorizationToken(token.authorizationToken);
        }

        return token;
      }),
      catchError((err: HttpErrorResponse) => {
        throw (err);
      })
    );
  }

  public isAuthenticated(): boolean {
    const token = this.getAuthorizationToken;
    const helper = new JwtHelperService();
    return !helper.isTokenExpired(token);
  }

  public setAuthorizationToken(token: string) {
    localStorage.setItem('authorizationToken', token);
  }

  get getAuthorizationToken(): string | null {
    return localStorage.getItem('authorizationToken') || null;
  }

  public logout() {
    localStorage.removeItem('authorizationToken');
    this.navService.goToPage(`${Routing.APP}`);
  }
}
