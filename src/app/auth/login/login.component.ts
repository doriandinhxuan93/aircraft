import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { IToken } from '../interfaces/token.interface';
import { NavService } from 'src/app/shared/services/nav.service';
import { Routing } from 'src/app/shared/enums/routing';
import { NotifierService } from 'angular-notifier';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as _ from "lodash";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy{

  private TAG = '[LoginComponent]';
  public loginForm: FormGroup;
  public errorMessage: string;
  public submitted: boolean = false;

  private _urlParams: Subscription;
  private state: string | null = null;

  constructor(
    private authService: AuthService,
    private navService: NavService,
    private notifierService: NotifierService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this._urlParams = this.route.queryParams.subscribe((params) => {
      console.log(params)
      this.state = _.get(params, 'url', null);
    });

    this.initLoginForm();
  }

  public initLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
    });
  }

  public onSubmit() {
    if (this.loginForm.invalid) return;

    this.submitted = true;
    const body = this.loginForm.getRawValue();
    this.authService.login(body).subscribe(
      (result) => {
        if (result) {
          console.log(`${this.TAG} onSubmit - success`, result);
          (this.state)
            ? this.navService.goToPage(`${this.state}`)
            : this.navService.goToPage(`${Routing.DASHBOARD}/${Routing.SETTINGS}`);
        }
      },
      (err) => {
        console.error(`${this.TAG} onSubmit - error`, err);
        this.submitted = false;
        this.errorMessage = err.error.message;
        this.notifierService.notify( 'error', err.error.message );
      },
      () => {
        this.submitted = false;
      });

  }

  ngOnDestroy(): void {
    if (this._urlParams)  this._urlParams.unsubscribe()
  }
}
