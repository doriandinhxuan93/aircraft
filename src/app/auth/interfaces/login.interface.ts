import { Roles } from 'src/app/shared/enums/roles';

export interface ILogin {
    email: string;
    password: string;
    confirmPassword?: string;
    role: Roles;
}
