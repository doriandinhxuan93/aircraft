import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): any {

        const token = this.authService.getAuthorizationToken;

        if (token) {
            return next.handle(
                request.clone({
                    headers: request.headers.append('Authorization', token)
                })
            );
        }

        return next.handle(request);
    }
}
