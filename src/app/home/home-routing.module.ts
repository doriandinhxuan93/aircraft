import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { HomeComponent } from './home.component';
import { FlightsDetailsComponent } from './flights-details/flights-details.component';

const routes: Routes = [
  {
    path: 'app', component: HomeComponent,
    children: [
      { path: '', component: MainPageComponent },
      { path: 'flights', component: FlightsListComponent },
      { path: 'details/:id', component: FlightsDetailsComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule { }
