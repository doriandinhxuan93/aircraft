import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';

import { SharedModule } from '../shared/shared.module';
import { MainPageComponent } from './main-page/main-page.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { NgDatepickerModule } from 'ng2-datepicker';
import { AgmCoreModule } from '@agm/core';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { HomeComponent } from './home.component';
import { FlightsDetailsComponent } from './flights-details/flights-details.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCvQfKHSDlkKhE4cySxTFS3v1JCd7LD6d4',
      libraries: ['places']
    }),
    NgDatepickerModule
  ],
  declarations: [MainPageComponent, SearchFormComponent, FlightsListComponent, HomeComponent, FlightsDetailsComponent]
})
export class HomeModule { }
