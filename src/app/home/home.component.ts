import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <div id="wrapper">
  <app-navbar></app-navbar>
  <router-outlet></router-outlet>
  <app-footer></app-footer>
  </div>
  `,
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
