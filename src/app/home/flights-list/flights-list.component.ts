import { Component, OnInit } from '@angular/core';
import { SearchFlightService } from '../services/search-flight.service';
import { Flight } from 'src/app/shared/entities/flight';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { SearchForm } from '../entities/search-form';

@Component({
  selector: 'home-flights-list',
  templateUrl: './flights-list.component.html',
  styleUrls: ['./flights-list.component.scss']
})
export class FlightsListComponent implements OnInit {
  
  private TAG: string = '[FlightsListComponent]';

  public flights: Flight[] = null;
  private flightsSub: Subscription;
  private routeSub: Subscription;
  private searchModel: SearchForm;

  constructor (
    private searchFlightService: SearchFlightService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.routeSub = this.route.params.subscribe(params => {
      console.log(params)
      this.searchModel = new SearchForm().parse({ takeoff: params['takeoff']});

      this.searchFlightService.searchFlight(this.searchModel).subscribe(
        (flights: Flight[]) => {
          console.log(`${this.TAG} search flight - successfully!`, flights);
          this.flights = flights;
        },
        (error) => {
          console.log(`${this.TAG} close route - failure!`, error)
        }
      );

      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy(): void {
    if(this.routeSub) this.routeSub.unsubscribe();
    if(this.flightsSub) this.flightsSub.unsubscribe();
  }
}