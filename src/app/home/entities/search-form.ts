import {Serialize} from "src/app/shared/interfaces/serialize";
import {Entity} from "src/app/shared/classes/entity";
import * as _ from "lodash";

export class SearchForm extends Entity implements Serialize<SearchForm>{

    public takeoff: string;
    public date: Date;

    parse(data: any): SearchForm {
        if (_.isNull(data)) throw "Invalid jwt token";

        this.takeoff = _.get(data, "takeoff");
        this.date = _.get(data, "date");

        console.log(`[SearchForm] Parsed successfully`, this);

        return this;
    }

    serialize(): any {
        let serialized = {
            takeoff: this.takeoff,
            date: this.date
        };

        return serialized;
    }
}