import { Injectable } from '@angular/core';
import { SearchForm } from '../entities/search-form';
import { Flight } from 'src/app/shared/entities/flight';
import * as F from 'src/app/shared/entities/mocukups/flights';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchFlightService {
  private TAG: string = "[SearchFlightService]";

  static FLIGHTS: string = 'flights/all';

  private flightsDataSource: BehaviorSubject<Flight[]> = new BehaviorSubject<Flight[]>(null);
  public flightsData: Observable<Flight[]> = this.flightsDataSource.asObservable();

  constructor(private http: HttpClient, private utilsService: UtilsService) { }

  searchFlight(body: SearchForm): Observable<any> {
    let endpoint = this.utilsService.interpolate(`${environment.api}${SearchFlightService.FLIGHTS}`, {});

    console.log(environment.api)

    return this.http.post(endpoint, body, this.utilsService.httpHeaders()).pipe(
      map((response) => {

        console.log(this.TAG, "[Response] getRoute", response);
        let flights = [];
        
        response['_body'].map(f => flights.push(new Flight().parse(f)));

        return flights;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
        })
    );
  }
}
