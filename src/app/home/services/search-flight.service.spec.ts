import { TestBed } from '@angular/core/testing';

import { SearchFlightService } from './search-flight.service';

describe('SearchFlightService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchFlightService = TestBed.get(SearchFlightService);
    expect(service).toBeTruthy();
  });
});
