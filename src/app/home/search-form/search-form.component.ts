import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchForm } from '../entities/search-form';
import { MapsAPILoader } from '@agm/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as plLocale from 'date-fns/locale/pl';
import { SearchFlightService } from '../services/search-flight.service';
import { Subscription } from 'rxjs';
import { Flight } from 'src/app/shared/entities/flight';
import { NavService } from 'src/app/shared/services/nav.service';

@Component({
  selector: 'home-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  private TAG: string = '[SearchFormComponent]';
  public searchForm: FormGroup;
  public model: SearchForm;
  public submitted: boolean = false;

  private searchFlightServiceSub: Subscription;

  public options: DatepickerOptions = {
    minYear: 2018,
    maxYear: 2030,
    displayFormat: 'D MMMM YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 1,
    minDate: new Date(Date.now()),
    addStyle: { width: '100%', border: 'none', 'font-size': '18px', 'padding-left': '40px'},
    locale: plLocale
  };


  @ViewChild('search') public searchElementRef: ElementRef;

  constructor(
    private formBuilder: FormBuilder, 
    private mapsAPILoader: MapsAPILoader,
    private searchFlightService: SearchFlightService,
    private navService: NavService) { }

  public ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      takeoff: [null, Validators.required],
      date: [new Date(Date.now()), Validators.required]
    });

    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
    });
  }


  public onSubmit() {
    this.submitted = true;

    if (this.searchForm.valid) {
      this.model = new SearchForm().parse(this.searchForm.value);
      this.navService.goToPage('app/flights', this.model);
    }
  }

  public selectedDay(){
    console.log(this.model);
    console.log(this.searchForm);
  }

  public ngOnDestroy(): void {
    if (this.searchFlightServiceSub) this.searchFlightServiceSub.unsubscribe();
  }
} 