import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SettingsComponent } from './settings/settings.component';
import { SharedModule } from '../shared/shared.module';
import { AddFlightComponent } from './add-flight/add-flight.component';
import { DashboardComponent } from './dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PanelComponent } from './panel/panel.component';


@NgModule({
  imports: [
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [SettingsComponent, AddFlightComponent, DashboardComponent, SidebarComponent, PanelComponent],
  bootstrap: [SettingsComponent]
})
export class DashboardModule { }
