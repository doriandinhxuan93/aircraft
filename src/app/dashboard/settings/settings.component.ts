import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Roles } from 'src/app/shared/enums/roles';
import { DashboardService } from '../services/dashboard.service';
import { License } from 'src/app/shared/entities/license';
import { Aircraft } from 'src/app/shared/entities/aircraft';
import { datepickerOptions } from 'src/app/shared/classes/datepicker-options';
import { NotifierService } from 'angular-notifier';
import * as _ from 'lodash';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/entities/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  private TAG = '[SettingsComponent';
  public settingsForm: FormGroup;

  public user: User;
  public formBuilder: any;
  private submitted: boolean = false;

  private user$: Subscription;

  get dtOptions() {
    return datepickerOptions;
  }

  constructor(
    private dashboardService: DashboardService,
    private notifierService: NotifierService,
    private userService: UserService) { }

  ngOnInit() {
    this.user$ = this.userService.user.subscribe(
      (user: User) => {
        this.user = user;
        this.initSettingsForm();
    });

  }

  get licenses() {
    return (this.settingsForm.controls.licenses as FormArray).controls;
  }

  get aircrafts() {
    return (this.settingsForm.controls.aircrafts as FormArray).controls;
  }

  form() {
    console.log(this.settingsForm);
  }

  private initSettingsForm() {
    this.settingsForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      surname:  new FormControl(null, Validators.required),
      email:  new FormControl(null, Validators.required),
      description: new FormControl(null),
      role: new FormControl(Roles.USER, Validators.required),
      flightHours: new FormControl(null),
      licenses: new FormArray([]),
      aircrafts: new FormArray([]),
      dateOfBirth: new FormControl(new Date(), Validators.required),
    });

    this.disabledField('email');
    this.disabledField('flightHours');

    this.settingsForm.patchValue(this.user);
    this.user.licenses.map((l: License) => this.licenses.push(this.initLicense(l.name, l.date)));
    this.user.aircrafts.map((l: Aircraft) => this.aircrafts.push(this.initAircraft(l.name, l.totalHours)));
  }

  private disabledField(field: string): void {
    this.settingsForm.controls[field].disable();
  }

  public initLicense(name?: string, date?: string): FormGroup {
    return new FormGroup({
      name: new FormControl((name) ? name : null),
      date: new FormControl((date) ? date : new Date())
    });
  }

  public addItemToLicense(): void {
    this.licenses.push(this.initLicense());
  }

  public removeLicense(index: number): void {
    if (index > -1) {
      this.licenses.splice(index, 1);
    }
  }

  public initAircraft(name?: string, totalHours?: number): FormGroup {
    return new FormGroup({
      name: new FormControl((name) ? name : null),
      totalHours: new FormControl(((totalHours) ? totalHours : 0), this.isNumber)
    });
  }

  public addItemToLAircrafts(): void {
    this.aircrafts.push(this.initAircraft());
  }

  public removeAircraft(index: number): void {
    if (index > -1) {
      this.aircrafts.splice(index, 1);
    }
  }

  public onSubmit() {
    if (this.settingsForm.invalid) return;
    this.submitted = true;

    const user = this.settingsForm.getRawValue();

    this.dashboardService.updateUser(user).subscribe(
      (result) => {
        if (result.code === 200) {
          console.log(`${this.TAG} onSubmit - success1`, result);
          this.notifierService.notify('success', 'Data has been changed');
          this.settingsForm.markAsPristine();
        }
      },
      (err) => {
        console.error(`${this.TAG} onSubmit - error`, err);
        this.notifierService.notify('error', err);
        this.submitted = false;
      },
      () => {
        this.submitted = false;
      });
  }

  get sumTotalFlightHours(): number {
    return  _.sumBy(this.aircrafts, (a) => parseInt(a.value.totalHours, 0));
  }

  private isNumber(input: AbstractControl): null | Object {
    return _.isNumber(input.value) ? null : { isNumber: true };
  }

  public ngOnDestroy(): void {
    if(this.user$) this.user$.unsubscribe();
  }
}
