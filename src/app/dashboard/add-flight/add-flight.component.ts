import { Component, OnInit, OnDestroy } from '@angular/core';
import { Flight } from 'src/app/shared/entities/flight';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DashboardService } from '../services/dashboard.service';
import { Subscription } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { UserService } from 'src/app/shared/services/user.service';
import { ThrowStmt } from '@angular/compiler';
import { User } from 'src/app/shared/entities/user';
import { datepickerOptions } from 'src/app/shared/classes/datepicker-options';
import { DifficultyOptions, Difficulty } from 'src/app/shared/enums/difficulty';
import { Aircraft } from 'src/app/shared/entities/aircraft';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.scss']
})
export class AddFlightComponent implements OnInit, OnDestroy {

  private TAG = '[AddFlightComponent]';
  public model: Flight = new Flight();
  public form: FormGroup;
  public submitted: boolean = false;
  public user: User;
  private __dashboardService: Subscription;
  private __user: Subscription;

  get dtOptions() {
    return datepickerOptions;
  }

  get difficultyOptions() {
    return DifficultyOptions;
  }

  constructor(
    private formBuilder: FormBuilder,
    private dashboardService: DashboardService,
    private notifierService: NotifierService,
    private userService: UserService) {}

  ngOnInit() {

    this.__user = this.userService.user.subscribe((user: User) => this.user = user);

    this.form = this.formBuilder.group({
      takeoff: ['', Validators.required],
      touchdown: ['', Validators.required],
      date: [new Date(), Validators.required],
      description: ['', Validators.required],
      time: [0, Validators.required],
      price: [0, Validators.required],
      aircraftId: ['', Validators.required],
      aircraftName: [''],
      difficulty: [Difficulty.EASY, Validators.required]
    });

    this.form.get('aircraftId').valueChanges.subscribe((id) => {
      const aircraft: Aircraft = _.find(this.user.aircrafts, (a: Aircraft) => a.id === id);
      this.form.controls['aircraftName'].setValue(aircraft.name);
    });
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) return;

    console.log(`${this.TAG} onSubmit parsed form`, this.form.value);

    this.__dashboardService = this.dashboardService.add(this.form.value).subscribe(
      (data: any) => {
        console.log(`${this.TAG} close route - succesfully!`, data);
       this.notifierService.notify('error', 'dassdsada');
        this.form.controls.status.markAsPristine();
      },
      (error) => {
        console.log(`${this.TAG} close route - failure!`, error);
        this.submitted = false;
      },
      () => {
        console.log(`${this.TAG} close route - completed!`);
        this.submitted = false;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.__user) this.__user.unsubscribe();
    if (this.__dashboardService) this.__dashboardService.unsubscribe();
  }
}
