import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { FlightService } from '../shared/services/flight.service';

@Component({
  selector: 'app-dashboard',
  template: `
    <div id="wrapper">
    <app-navbar></app-navbar>
    <router-outlet></router-outlet>
    </div>
  `,
})
export class DashboardComponent implements OnInit {

  constructor(
    private userService: UserService,
    private flightService: FlightService) {
    
  }

  ngOnInit() {
    this.userService.my().subscribe();
    this.flightService.my().subscribe();
  }
}

