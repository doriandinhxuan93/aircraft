import { Injectable } from '@angular/core';
import { Flight } from 'src/app/shared/entities/flight';
import { Observable, of } from 'rxjs';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Aircraft } from 'src/app/shared/entities/aircraft';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  static ADD_FLIGHT = 'flights/add';
  static UPDATE_USER = 'user/update';
  static USER = 'user/my';

  private TAG = '[DashboardService]';


  constructor(private utilsService: UtilsService, private http: HttpClient) { }

  add(body: Flight): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${DashboardService.ADD_FLIGHT}`, {});

    return this.http.post(endpoint, body, this.utilsService.httpHeaders()).pipe(
      map((response) => {
        console.log(this.TAG, '[Response] getRoute', response);
        return response;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
      })
    );
  }

  updateUser(user: any): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${DashboardService.UPDATE_USER}`, {});
    user.aircrafts = _.filter(user.aircrafts, (a: Aircraft) => !_.isNull(a.name));

    return this.http.put(endpoint, user, this.utilsService.httpHeaders()).pipe(
      map((response) => {

        console.log(this.TAG, '[Response] getRoute', response);

        return response;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
      })
    );
  }

  my(): Observable<any> {
    const endpoint = this.utilsService.interpolate(`${environment.api}${DashboardService.USER}`, {});

    return this.http.get(endpoint, this.utilsService.httpHeaders()).pipe(
      map((response) => {
        console.log(this.TAG, '[Response] user', response);
        return response;
      }),
      catchError((err: HttpErrorResponse) => {
        return of(err);
      })
    );
  }

}
