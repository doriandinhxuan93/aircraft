import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings/settings.component';
import { AddFlightComponent } from './add-flight/add-flight.component';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../auth/guard/auth.guard';
import { PanelComponent } from './panel/panel.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: SettingsComponent, canActivate: [AuthGuard] },
      { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
      { path: 'add', component: AddFlightComponent, canActivate: [AuthGuard] },
      { path: 'panel', component: PanelComponent, canActivate: [AuthGuard] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
