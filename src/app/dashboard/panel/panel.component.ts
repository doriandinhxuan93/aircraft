import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/shared/entities/user';
import { UserService } from 'src/app/shared/services/user.service';
import { Flight } from 'src/app/shared/entities/flight';
import { FlightService } from 'src/app/shared/services/flight.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  public user: User;
  private __user: Subscription;

  public flights: Flight[];
  private __flights: Subscription;

  constructor(private userService: UserService, private flightsService: FlightService) { }

  ngOnInit() {
    this.__user = this.userService.user.subscribe((user: User) => this.user = user);

    this.__flights = this.flightsService.flights.subscribe((flights: Flight[]) => {
      console.log(flights)
      this.flights = flights;
      console.log(this.flights.length);

    });


  }
}
