import {Component, OnInit} from '@angular/core';
import { NavService } from './shared/services/nav.service';
import { Routing } from './shared/enums/routing';
@Component({
  selector: 'app-root',
  template: `<notifier-container></notifier-container><router-outlet></router-outlet>`,
})
export class AppComponent implements OnInit {
  constructor(private navService: NavService) {}

  ngOnInit(): void {}
}
